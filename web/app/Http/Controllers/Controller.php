<script language="php">

class Controller{

	public function view($name , $params = array()){
		$template = file_get_contents(getcwd().'/../resources/views/'.$name.'.blade.php');

		foreach ($params as $key => $value){
			$template = str_replace('{'.$key.'}' , $value , $template);
		}

		return $template;
	}

}

