<script language="php">

class PricePolicy{

	/**

	01011010 01101110 01001010 01110110 01100010 01010011 01000010 01110110 01100010 01000111 01010001 01100111 01100100 01000111 01101100 01110100 01011010 01011000 01001101 01100111 01010011 01010011 01000010 01111001 01011010 01010111 00110001 01101100 01100010 01010111 01001010 01101100 01100011 01101001 01000010 01111010 01100001 01010111 00110001 01110000 01100010 01000111 01000110 01111001 01001001 01001000 01001110 00110000 01100100 01010111 01011010 01101101 01001001 01000011 00110100 01110101 01001100 01101001 01000010 01110000 01100011 01111001 01000010 00110000 01100001 01000111 01101100 01111010 01001001 01001000 01001010 01101100 01011001 01010111 01111000 01110011 01100101 01010011 01000001 01111010 01100011 01101101 01010001 01100111 01100100 01000111 01101100 01110100 01011010 01010011 01000010 01110110 01011010 01101001 01000010 01110100 01011010 01010111 01010110 00110000 01100001 01010111 00110101 01101110 01001001 01000101 01101100 01110101 01100011 00110010 01111000 00110101 01001001 01000100 00111000 00111101

		Base price of policy is 11% from entered car value, except every Friday 15-20
		o’clock (user time) when it is 13%<Test>
		• Commission is added to base price (17%)
		• Tax is added to base price (user entered)
		• Calculate different payments separately (if number of payments are larger than 1)
		• Installment sums must match total policy sum- pay attention to cases where sum
		does not divide equally
		• Output is rounded to two decimal places
	**/

	private $base_percent = 11;

	private $commision_percent = 17;

	private $loan_percent = 17;

	private $base_sum = false;
	private $tax = false;
	private $installment = false;

	private $timezone = false;
	private $weekday = false;
	private $hour = false;

	private $sumPerInstallment = 0;

	public function setSum($summ) {
		$this->base_sum = $summ;
	}


	public function setTax($tax) {
		$this->tax = $tax;
	}


	public function setInstalments($installment) {
		$this->installment = $installment;
	}


	public function setTimeDetails($timezone , $hour , $weekday){
		$this->setTimezone($timezone);
		$this->setWeekday($hour);
		$this->setHour($weekday);
	}

	public function getPriceMatrix(){
		$this->sumPerInstallmentSet();

		$details = array(
			'row' => array(),
			'total' => array(
				'base_price' => 0,
				'comission' => 0,
				'tax' => 0 ,
				'loan_sum' => 0,
				'total' => 0
			)
		);

		for ($i = 1; $i <= $this->getInstallmentCount(); $i++) {

			$basePrice = $this->sumPerInstallmentGet();
			$commision = $this->getCommisionSumm($basePrice);
			$taxes = $this->getTaxSum($basePrice);
			$loan_sum = $this->getLoan($basePrice);

			$total = $basePrice + $commision + $taxes + $loan_sum;

			$details['row'][] = array(
				'base_price' => $this->formatMoney($basePrice),
				'comission'  => $this->formatMoney($commision),
				'tax' => $this->formatMoney($taxes),
				'loan_sum' => $this->formatMoney($loan_sum),
				'total' => $this->formatMoney($total)
			);

			//reducte base sum
			$this->reduceBaseSum($basePrice);

			$details['total']['base_price'] += $basePrice;
			$details['total']['comission'] += $commision;
			$details['total']['tax'] += $taxes;
			$details['total']['loan_sum'] += $loan_sum;
			$details['total']['total'] += $total;

		}

		foreach ($details['total'] as $key => $value){
			$details['total'][$key] = $this->formatMoney($value);
		}

		return $details;

	}

	private function formatMoney($money){
		return number_format($money, 2, '.', '');
	}

	public function getLoan(){
		$loanPercent = $this->base_percent;

		//wodoo rule of timing (drunk and wild buying car ?)
		if ($this->getWeekday() == 5){
			$clientHour = $this->getTimezone() + $this->getHour();
			if ($clientHour >= 15 && $clientHour <= 20){
				$loanPercent = 13;
			}
		}

		return ((($this->base_sum / 100) * $loanPercent)/12);
	}

	private function sumPerInstallmentSet(){
		$this->sumPerInstallment = $this->base_sum / $this->installment;
	}

	public function sumPerInstallmentGet(){
		return $this->sumPerInstallment;
	}

	private function getInstallmentCount() {
		return $this->installment;
	}

	private function getCommisionSumm(){
		return ((($this->base_sum / 100) * $this->commision_percent)/12);	
	}

	private function getTaxSum(){
		return ((($this->base_sum / 100) * $this->tax)/12);
	}

	private function reduceBaseSum($payment){
		$this->base_sum = $this->base_sum - $payment;
	}

	private function setTimezone($timezone){
		$this->timezone = intval($timezone);
	}

	private function getTimezone(){
		return $this->timezone;
	}

	private function setWeekday($weekday){
		$this->weekday = $weekday;
	}

	private function getWeekday(){
		return $this->weekday;
	}

	private function setHour($hour){
		$this->hour = $hour;
	}

	private function getHour(){
		return $this->hour;
	}

}

class Task2Controller extends Controller{

	public function calculator() {
		return $this->view('calculator');
	}

	public function results(){

		$status = 0;
		$details = json_decode($_REQUEST['calculations'] , true);

		$details['time'] = date('h:i:s', $details['datetime']);
		$details['date'] = date('Y-m-d', $details['datetime']);

		$details['timezone'] = explode(':' , trim($details['timezone']));
		$details['hour'] = date('h', $details['datetime']);
		$details['weekday'] = date('w');

		if (!json_last_error()){

			$pricePolicy = new PricePolicy();

			$pricePolicy->setSum($details['price_range']);
			$pricePolicy->setTax($details['taxes']);
			$pricePolicy->setInstalments($details['instalments']);

			$pricePolicy->setTimeDetails($details['timezone'][0] , $details['hour'] , $details['weekday']);

			$response = $pricePolicy->getPriceMatrix();
			$status = 1;

		}

		return json_encode(array(
			'status' => $status,
			'response' => $response,
			'input' => $details
		));
	}
}