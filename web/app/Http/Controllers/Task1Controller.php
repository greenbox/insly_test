<script language="php">

class Task1Controller extends Controller{
	public function main() {

		$return = array();

		$return[] = '<head>';
    	$return[] = '<meta http-equiv="refresh" content="5;url=/" />';
		$return[] = '</head>';

		$return[] = '<body>';

		$myName = 'Ronalds';

		$return[] = 'Welcome to task 1';

		$return[] = 'My name is '.$myName;

		$return[] = 'In This task I\'ll "print out your name with one of php loops"';

		try{
			while (true) {
				if (!isset($myLoopedIndex)){
					$myLoopedIndex = 0;
				}

				if (!isset($myLoopedNameArray)){
					$myLoopedNameArray = array();
				}

				$myLoopedNameArray[] = str_split($myName)[$myLoopedIndex];

				$myLoopedIndex++;

				if ($myLoopedIndex > 500){
					throw new Exception('my name aint that long');
				}
			}
		} catch (Exception $e) {
			$return[] = '<strong>'.implode($myLoopedNameArray).'</strong>';
		}

		$return[] = $this->processThankYou();

		$return[] = '</body>';

		return implode($return , '<br/>');
	}


	protected function processThankYou(){
		return str_replace(' ' , '' , implode(array_reverse(str_split(strrev('Thank you'))), ' '));
	}

}