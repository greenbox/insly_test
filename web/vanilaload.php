<?php


class CompactErrorHandler{

	private static $me;

	public static function detectAndDisplay(){

		$return = false;

		if (!isset($this)){
			self::$me = new self;
			$return = self::$me->lazyDetector();
		}else{
			$return = $this->lazyDetector();
		}

		return $return;
	}

	private function lazyDetector(){
		//me don't care
		http_response_code(404);
		die(
			'<html><head><title>This is 404 or any other error</title></head><body><h1>ERRROR</h3><iframe width="560" height="315" src="https://www.youtube.com/embed/PhN8v0whI0o?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></body></html>'

		);	
	}
}

if (!isset($_REQUEST['subcontroller'])){
	$welcome = file_get_contents(getcwd().'/../resources/views/welcome.blade.php');
	die($welcome);
}

//Router !!!! must think something wise ! Try cache - this is how smart people do it :P 
TRY {

	$mainController = file_get_contents(getcwd().'/../app/Http/Controllers/Controller.php', true);

	$controllerData = file_get_contents(getcwd().'/../app/Http/Controllers/'.ucfirst($_REQUEST['subcontroller'].'Controller.php'), true);	

	IF (!$controllerData || !isset($_REQUEST['action'])){
		CompactErrorHandler::detectAndDisplay();
	}ELSE{

		if (isset($debug)){
			die (str_replace('<script language="php">', '', $mainController.$controllerData.' $controller = new '.ucfirst($_REQUEST['subcontroller']).'Controller(); die($controller->'.$_REQUEST['action'].'());'));
		}

		eval(str_replace('<script language="php">', '', $mainController.$controllerData.' $controller = new '.ucfirst($_REQUEST['subcontroller']).'Controller(); die($controller->'.$_REQUEST['action'].'());'));
	}
} CATCH (Exception $e) {
	CompactErrorHandler::detectAndDisplay();
}