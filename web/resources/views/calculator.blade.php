<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Calculator</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="/">
                       Vanilla Task #2
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">

                        <li><a href="/">Back</a></li>
                    </ul>
                </div>
            </div>
        </nav>

		<div class="container">
		    <div class="row">

		        <div class="col-md-12">
		            <div class="panel panel-default">
		                <div class="panel-heading">Calculator</div>

		                <div class="panel-body">

							<div class="form-group row">
							  <div class="col-xs-4">
							    <label for="ex3">Estimated value of the car (<span id="estimated_value">50000</span> EUR)</label>
							    <input type="range" id="price_range" class="form-control-range" id="formControlRange" onchange="processEstimate(this);">
							  </div>
							  <div class="col-xs-2">
							    <label for="taxes">Tax percentage (<span id="taxes_value">50</span> %)</label>
							    <input type="range" id="taxes" class="form-control-range" id="formControlRange" onchange="processTaxes(this);">
							  </div>
							  <div class="col-xs-3">
							    <label for="instalments">Number of instalments (<span id="installment_value">6</span> Months)</label>
							    <input type="range" id="instalments" class="form-control-range" id="formControlRange" onchange="processInstallments(this);">
							  </div>
							  <div class="col-xs-3">
							    <label for="calculate">Processor</label>
							    <button type="button" id="calculate" onclick="calculateButton()" class="btn btn-default">Calculate</button>
							  </div>
							</div>


							<script type="text/javascript">
								function processEstimate(self){
									document.getElementById("estimated_value").innerHTML = (parseInt(self.value) * 100);
								}

								function processTaxes(self) {
									document.getElementById("taxes_value").innerHTML = (parseInt(self.value));
								}

								function processInstallments(self) {
									
									monthValue = parseInt(parseInt(self.value)/8.3);
									if (!monthValue){
										monthValue = 1;
									}
									document.getElementById("installment_value").innerHTML = monthValue;
								}

								function getTimeZone() {
								    var offset = new Date().getTimezoneOffset(), o = Math.abs(offset);
								    return (offset < 0 ? "+" : "-") + ("00" + Math.floor(o / 60)).slice(-2) + ":" + ("00" + (o % 60)).slice(-2);
								}

								function calculateButton() {

									var instalmentCount = parseInt(parseInt(document.getElementById("instalments").value)/8.3);

									if (instalmentCount == 0){
										instalmentCount = 1;
									}

									var calculateDetails = JSON.stringify({
										price_range : (parseInt(document.getElementById("price_range").value) * 100),
										taxes : parseInt(document.getElementById("taxes").value),
										instalments : instalmentCount,
										datetime : (Date.now()/1000),
										timezone : getTimeZone()

									});

									var xhttp = new XMLHttpRequest();

									xhttp.open("POST", "debug.php?subcontroller=task2&action=results&debug", true);
									xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


									xhttp.onreadystatechange = function() {
										if (xhttp.readyState === 4) {
											var responseFull = JSON.parse(xhttp.response);
											if (responseFull.status == 1){

												var loanTable = document.getElementById("paymentTableHolder");
    											loanTable.classList.add("hidden");

    											//clear old data
   												document.getElementById("loadDetailTable").innerHTML = '';

   												var rowHtml ='' , rowIndex = '';

   												console.log(responseFull);

   												for (var i = 0; i < responseFull.response.row.length; i++){

   													//lazy mode of getting data

   													rowIndex = (i + 1);

   													rowHtml = '<tr><th scope="row">'+ rowIndex +'</th>';
													rowHtml += '<td>'+ responseFull.response.row[i].base_price +'</td>';
													rowHtml += '<td>'+ responseFull.response.row[i].comission +'</td>';
													rowHtml += '<td>'+ responseFull.response.row[i].loan_sum+'</td>';
													rowHtml += '<td>'+ responseFull.response.row[i].tax +'</td>';
													rowHtml += '<td>'+ responseFull.response.row[i].total +'</td>';
													rowHtml += '</tr>';

													document.getElementById("loadDetailTable").innerHTML += rowHtml; 


   												}

   												//add all totals
   												document.getElementById("total_base_price"
   													).innerHTML = responseFull.response.total.base_price;
   												document.getElementById("total_comission").innerHTML = responseFull.response.total.comission;
   												document.getElementById("total_loan_sum").innerHTML = responseFull.response.total.loan_sum;
   												document.getElementById("total_tax").innerHTML = responseFull.response.total.tax;
   												document.getElementById("total").innerHTML = responseFull.response.total.total;

								      			//display table holder
								      			document.getElementById("paymentTableHolder");
    											loanTable.classList.remove("hidden");

											}
										}
									}

									xhttp.send("calculations="+calculateDetails);


								}

							</script>


		                </div>
		            </div>
		        </div>


		        <div id="paymentTableHolder" class="col-md-12 hidden">
		            <div class="panel panel-default">
		                <div class="panel-heading">Payment Details</div>
		                	<div class="panel-body">

								<table class="table">
								  <thead>
								    <tr>
								      <th scope="col">#</th>
								      <th scope="col">Base Price</th>
								      <th scope="col">Comission</th>
								      <th scope="col">Loan sum</th>
								      <th scope="col">Tax</th>
								      <th scope="col">Total</th>
								    </tr>
								  </thead>
								  <tbody id="loadDetailTable">
								    <tr>
								      <th scope="row">1</th>
								      <td>base_price</td>
								      <td>comission</td>
								      <td>loan_sum</td>
								      <td>tax</td>
								      <td>total</td>
								    </tr>
								  </tbody>
								</table>

								<table class="table">
								  <thead>
								    <tr>
								      <th scope="col">#</th>
								      <th scope="col">Total Base Price</th>
								      <th scope="col">Total Comission</th>
								      <th scope="col">Total Loan sum</th>
								      <th scope="col">Total Tax</th>
								      <th scope="col">Total Total</th>
								    </tr>
								  </thead>
								  <tbody id="loadDetailTable">
								    <tr>
								      <th scope="row"></th>
								      <td id="total_base_price"></td>
								      <td id="total_comission"></td>
								      <td id="total_loan_sum"></td>
								      <td id="total_tax"></td>
								      <td id="total"></td>
								    </tr>
								  </tbody>
								</table>

		                </div>
		            </div>
		        </div>



		    </div>
		</div>
    </div>
</body>
</html>



