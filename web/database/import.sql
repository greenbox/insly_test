-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.41-0ubuntu0.14.04.1 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table insly.activity_log
CREATE TABLE IF NOT EXISTS `activity_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL,
  `creator_id` int(10) unsigned NOT NULL,
  `action` enum('create','update') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'create',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activity_log_person_id_foreign` (`person_id`),
  KEY `activity_log_creator_id_foreign` (`creator_id`),
  CONSTRAINT `activity_log_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `system_users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `activity_log_person_id_foreign` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table insly.activity_log: ~0 rows (approximately)
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;


-- Dumping structure for table insly.address
CREATE TABLE IF NOT EXISTS `address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `address_person_id_foreign` (`person_id`),
  CONSTRAINT `address_person_id_foreign` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table insly.address: ~0 rows (approximately)
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
/*!40000 ALTER TABLE `address` ENABLE KEYS */;


-- Dumping structure for table insly.education
CREATE TABLE IF NOT EXISTS `education` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(10) unsigned NOT NULL,
  `edu_instance_id` int(10) unsigned NOT NULL,
  `person_id` int(10) unsigned NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `education_language_id_foreign` (`language_id`),
  KEY `education_edu_instance_id_foreign` (`edu_instance_id`),
  KEY `education_person_id_foreign` (`person_id`),
  CONSTRAINT `education_person_id_foreign` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE,
  CONSTRAINT `education_edu_instance_id_foreign` FOREIGN KEY (`edu_instance_id`) REFERENCES `instance` (`id`) ON DELETE CASCADE,
  CONSTRAINT `education_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table insly.education: ~0 rows (approximately)
/*!40000 ALTER TABLE `education` DISABLE KEYS */;
/*!40000 ALTER TABLE `education` ENABLE KEYS */;


-- Dumping structure for table insly.instance
CREATE TABLE IF NOT EXISTS `instance` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(10) unsigned NOT NULL,
  `person_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `instance_language_id_foreign` (`language_id`),
  KEY `instance_person_id_foreign` (`person_id`),
  KEY `instance_name_index` (`name`),
  CONSTRAINT `instance_person_id_foreign` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE,
  CONSTRAINT `instance_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table insly.instance: ~0 rows (approximately)
/*!40000 ALTER TABLE `instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `instance` ENABLE KEYS */;


-- Dumping structure for table insly.introduction
CREATE TABLE IF NOT EXISTS `introduction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(10) unsigned NOT NULL,
  `person_id` int(10) unsigned NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `introduction_language_id_foreign` (`language_id`),
  KEY `introduction_person_id_foreign` (`person_id`),
  CONSTRAINT `introduction_person_id_foreign` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE,
  CONSTRAINT `introduction_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table insly.introduction: ~0 rows (approximately)
/*!40000 ALTER TABLE `introduction` DISABLE KEYS */;
/*!40000 ALTER TABLE `introduction` ENABLE KEYS */;


-- Dumping structure for table insly.language
CREATE TABLE IF NOT EXISTS `language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table insly.language: ~0 rows (approximately)
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
/*!40000 ALTER TABLE `language` ENABLE KEYS */;


-- Dumping structure for table insly.persons
CREATE TABLE IF NOT EXISTS `persons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birth_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `person_ident` int(11) NOT NULL,
  `creator_id` int(10) unsigned NOT NULL,
  `is_employee` tinyint(1) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persons_email_unique` (`email`),
  KEY `persons_person_ident_index` (`person_ident`),
  KEY `persons_creator_id_foreign` (`creator_id`),
  CONSTRAINT `persons_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `system_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table insly.persons: ~0 rows (approximately)
/*!40000 ALTER TABLE `persons` DISABLE KEYS */;
/*!40000 ALTER TABLE `persons` ENABLE KEYS */;


-- Dumping structure for table insly.system_users
CREATE TABLE IF NOT EXISTS `system_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `person_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `system_users_person_id_index` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table insly.system_users: ~0 rows (approximately)
/*!40000 ALTER TABLE `system_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_users` ENABLE KEYS */;


-- Dumping structure for table insly.work_experience
CREATE TABLE IF NOT EXISTS `work_experience` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `person_id` int(10) unsigned NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `work_experience_language_id_foreign` (`language_id`),
  KEY `work_experience_company_id_foreign` (`company_id`),
  KEY `work_experience_person_id_foreign` (`person_id`),
  CONSTRAINT `work_experience_person_id_foreign` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE,
  CONSTRAINT `work_experience_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `instance` (`id`) ON DELETE CASCADE,
  CONSTRAINT `work_experience_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table insly.work_experience: ~0 rows (approximately)
/*!40000 ALTER TABLE `work_experience` DISABLE KEYS */;
/*!40000 ALTER TABLE `work_experience` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;


INSERT INTO `system_users` (`username`, `password`, `person_id`, `remember_token`, `created_at`, `updated_at`)
VALUES ('sovas', 'sovas', '1', NULL, NULL, NULL);

INSERT INTO `persons` (`first_name`, `last_name`, `birth_date`, `person_ident`, `creator_id`, `is_employee`, `email`, `phone_number`, `created_at`, `updated_at`)
VALUES ('Ronalds', sha1('Sovas'), '1986-03-07 04:00', '213', '1', '1', 'sovas@greenbox.lv', '29886586', NULL, NULL);

INSERT INTO `address` (`person_id`, `street`, `city`, `country`, `created_at`, `updated_at`)
VALUES ('1', 'Rigas iela 1', 'Riga', 'Latvija', NULL, NULL);

INSERT INTO `language` (`language`, `country_code`, `country`, `created_at`, `updated_at`)
VALUES ('English', 'en', 'England', NULL, NULL);

INSERT INTO `language` (`language`, `country_code`, `country`, `created_at`, `updated_at`)
VALUES ('Spanish', 'sp', 'Spain', NULL, NULL);

INSERT INTO `language` (`language`, `country_code`, `country`, `created_at`, `updated_at`)
VALUES ('France', 'fr', 'France', NULL, NULL);

INSERT INTO `introduction` (`language_id`, `person_id`, `description`, `created_at`, `updated_at`)
VALUES ('1', '1', 'I would like to make a timestamp column with a default value of CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP using the Laravel Schema Builder/Migrations. I have gone through the Laravel documentation several times and I don\'t see how I can make that the default for a timestamp column.', NULL, NULL);

INSERT INTO `introduction` (`language_id`, `person_id`, `description`, `created_at`, `updated_at`)
VALUES ('2', '1', 'Me gustaría crear una columna de marca de tiempo con un valor predeterminado de CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP mediante el Laravel Schema Builder / Migrations. He revisado la documentación de Laravel varias veces y no veo cómo puedo hacer que sea el predeterminado para una columna de marca de tiempo.', NULL, NULL);

INSERT INTO `introduction` (`language_id`, `person_id`, `description`, `created_at`, `updated_at`)
VALUES ('3', '1', 'Je voudrais créer une colonne timestamp avec la valeur par défaut CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP à laide de Laravel Schema Builder / Migrations. Jai parcouru la documentation de Laravel à plusieurs reprises et je ne vois pas comment je pourrais en faire la valeur par défaut pour une colonne timestamp.', NULL, NULL);

INSERT INTO `instance` (`language_id`, `person_id`, `name`, `description`, `created_at`, `updated_at`)
VALUES ('1', '1', 'title', '', NULL, NULL);

INSERT INTO `instance` (`language_id`, `person_id`, `name`, `description`, `created_at`, `updated_at`)
VALUES ('2', '1', 'título', '', NULL, NULL);

INSERT INTO `instance` (`language_id`, `person_id`, `name`, `description`, `created_at`, `updated_at`)
VALUES ('3', '1', 'titre', '', NULL, NULL);

INSERT INTO `education` (`language_id`, `edu_instance_id`, `person_id`, `description`, `start_date`, `end_date`, `created_at`, `updated_at`)
VALUES ('1', '1', '1', 'I don\'t need no arms around me \r\nAnd I don\'t need no drugs to calm me ', '1986-03-07 05:00', '1986-03-07 06:00', NULL, NULL);

INSERT INTO `education` (`language_id`, `edu_instance_id`, `person_id`, `description`, `start_date`, `end_date`, `created_at`, `updated_at`)
VALUES ('2', '2', '1', 'No necesito brazos a mi alrededor\r\nY no necesito drogas para calmarme.', '1986-03-07 05:00', '1986-03-07 06:00', NULL, NULL);

INSERT INTO `education` (`language_id`, `edu_instance_id`, `person_id`, `description`, `start_date`, `end_date`, `created_at`, `updated_at`)
VALUES ('3', '3', '1', 'Je n\'ai pas besoin de bras autour de moi\r\nEt je n\'ai pas besoin de drogue pour me calmer', '1986-03-07 05:00', '1986-03-07 06:00', NULL, NULL);


INSERT INTO `work_experience` (`language_id`, `company_id`, `person_id`, `description`, `start_date`, `end_date`, `created_at`, `updated_at`)
VALUES ('1', '1', '1', 'I went wheels up on a runway \r\nAnd that ticket was a long way', '1986-03-07 06:00', '1986-03-07 07:00', NULL, NULL);


INSERT INTO `work_experience` (`language_id`, `company_id`, `person_id`, `description`, `start_date`, `end_date`, `created_at`, `updated_at`)
VALUES ('1', '2', '2', 'Fui con las ruedas arriba en una pista \ r \ ny ese boleto estaba muy lejos', '1986-03-07 06:00', '1986-03-07 07:00', NULL, NULL);

INSERT INTO `work_experience` (`language_id`, `company_id`, `person_id`, `description`, `start_date`, `end_date`, `created_at`, `updated_at`)
VALUES ('1', '3', '3', 'Je suis monté en roue sur une piste \ r \ nEt ce billet était un long chemin', '1986-03-07 06:00', '1986-03-07 07:00', NULL, NULL);
